FROM node:7.8-alpine
COPY . /app
RUN cd /app && \
    apk  add --update --no-cache make gcc g++ python linux-headers udev && \
    npm install -s serialport --build-from-source=serialport && \
    npm install -s --production && \	 
    apk del make gcc g++ python linux-headers udev && \
    rm -rf  /var/cache/apk/* /app/assets

EXPOSE 3000
WORKDIR /app
CMD npm start
