var five = require('johnny-five')

// initialize the arduino board
var newBoard = ()=>{ return new five.Board({
    port: "/dev/ttyUSB0",
    repl: false,
    debug: true
  })}

  // initialize arduino board, setting the the desired USB port
  var setNewBoard = (port)=>{ return new five.Board({
    port: port,
    repl: false,
    debug: false
  })}

  var board = null

module.exports = {
    newBoard: newBoard ,
    setNewBoard:setNewBoard,
    board: board
}