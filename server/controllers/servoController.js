var five = require("johnny-five");

// Initialize the PWM pins in arduino board
var newPWM = function (board, pin) {
    board.pinMode(pin, five.Pin.PWM);
}

// Map the provided value from range 0-180 to 0-250
var mapValfrom0_180to0_255 =  function (val) {
    val = five.Fn.map(val, 0, 180, 0, 255);
    return val;
}

// Move the motors attatched to the specified PWM pins 
var runPWMs =  function (board, pins = [] , ranges = []) {
    pins.map((pin, idx) => {
        board.analogWrite(pin, ranges[idx])
    })
}

// move the motor attached to the specified PWM pin
var runPWM = function (board, pin , range) {
    board.analogWrite(pin, range)
}

// Initialize the all PWM servo pins to arduino board
var INIT_PWM = (board,servoTubePins,allServoPins,pinUnderBox)=>{
    allServoPins.map((pin) => {
        newPWM(board, pin)
        
      })
      runPWMs(board, servoTubePins, [mapValfrom0_180to0_255(85),mapValfrom0_180to0_255(85)])
      runPWM(board, pinUnderBox, mapValfrom0_180to0_255(60))
      
}
module.exports = {
    newPWM: newPWM,
    mapValfrom0_180to0_255: mapValfrom0_180to0_255,
    runPWMs: runPWMs,
    runPWM: runPWM,
    INIT_PWM: INIT_PWM
}

