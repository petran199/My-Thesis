var servoController = require('./servoController')
var stepperController = require('./stepeprController')
var boardController = require('./boardController')

module.exports = {

	servoCnrl: servoController,
	stepperCtrl: stepperController,
	boardCtrl: boardController
		
}
