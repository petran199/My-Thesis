var express = require('express')
var router = express.Router()

// Example of using api routes
router.get('/:resource', function(req, res, next){
	var resource = req.params.resource
	
	res.json({
		confirmation: 'success',
		resource: resource
	})
})


module.exports = router