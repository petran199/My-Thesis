var { Promise } = require('bluebird')
var { servoCnrl } = require('../controllers')
var five = require('johnny-five')
var { boardVars } = require('../variables')

module.exports = {
  // smallPromise is used while no object has been recognised
  smallPromise: (resp, board, cb) => {
    return new Promise((resolve, reject) => {
      console.log("on nothing connected: ")
      var response = resp //"Nothing"

      board.wait(1000, () => {
        servoCnrl.runPWM(board, boardVars.servoTubePins[1], boardVars.rangeVal4)
        board.wait(1000, () => {
          servoCnrl.runPWM(board, boardVars.servoTubePins[0], boardVars.rangeVal2)
          board.wait(1000, () => {
            servoCnrl.runPWM(board, boardVars.servoTubePins[0], boardVars.rangeVal1)
            board.wait(1000, () => {
              servoCnrl.runPWM(board, boardVars.servoTubePins[1], boardVars.rangeVal3)
            })
          })
        })
      })
      // servoCnrl.runPWM(board,boardVars.servoTubePins[0],boardVars.)
      // servoCnrl.runPWMs(board, boardVars.servoTubePins, boardVars.evenRange2_4)
      // board.wait(1000, () => {
      // servoCnrl.runPWMs(board, boardVars.servoTubePins,boardVars.oddRange1_3)
      // resolve(response)
      // })
    })
      .then((response) => {
        cb(response)
      }, (err) => {
        console.log("Error ocured at smallPromise: " + err)
        cb("Server crashed")
      })
  },
  // bigPromise is used when object has been recognised and concequently we want to store it in the correct cart
  bigPromise: (FruitType, newPosition, board, stepper, cb) => {
    return new Promise((resolve, reject) => {
      var response = false
      let diafora = boardVars.lastPosition - newPosition
      if(boardVars.oneTimeRun){
        boardVars.oneTimeRun = false;
        boardVars.lastPosition = newPosition;
        diafora = 0;
      }
      console.log(FruitType + ", newPos: " + newPosition + ", lastPos: " + boardVars.lastPosition + " diff: " + diafora)

      if (diafora == 0) {
        console.log("diafora = 0 ")
        board.wait(500, () => {
          servoCnrl.runPWM(board, boardVars.pinUnderBox, boardVars.servoRangeUnderBox2)
          board.wait(1000, () => {
            servoCnrl.runPWM(board, boardVars.pinUnderBox, boardVars.servoRangeUnderBox1)
            resolve(response)
          })
        })
      }
      else if (diafora < 0) {
        let absDiafora = diafora * (-1);
        console.log("diafora < 0 , absDIf: " + absDiafora)
        board.wait(500, () => {
          stepper.step({
            steps: absDiafora,
            direction: five.Stepper.DIRECTION.CCW
          }, function () {
            servoCnrl.runPWM(board, boardVars.pinUnderBox, boardVars.servoRangeUnderBox2)
            board.wait(1000, () => {
              servoCnrl.runPWM(board, boardVars.pinUnderBox, boardVars.servoRangeUnderBox1)
              boardVars.lastPosition = newPosition
              resolve(response)
            })
          })
        })
      }
      else {
        console.log("diafora > 0 ")
        board.wait(500, () => {
          stepper.step({
            steps: diafora,
            direction: five.Stepper.DIRECTION.CW
          }, function () {
            servoCnrl.runPWM(board, boardVars.pinUnderBox, boardVars.servoRangeUnderBox2)
            board.wait(1000, () => {
              servoCnrl.runPWM(board, boardVars.pinUnderBox, boardVars.servoRangeUnderBox1)
              boardVars.lastPosition = newPosition
              resolve(response)
            })
          })
        })
      }
    })
      .then((response) => {
        cb(response)
      }, (err) => {
        console.log("Error ocured at bigPromise: " + err)
        cb("Server crashed")
      })
  }
}