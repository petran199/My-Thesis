var { servoCnrl } = require('../controllers')

const stepsPerRev = 100
const stepperPins = [5, 7, 6, 8]
const servoTubePins = [11, 10]
const allServoPins = [11, 10, 9]

const rangeVal1 = servoCnrl.mapValfrom0_180to0_255(85) //pin 11 pros ta mesa
const rangeVal2 = servoCnrl.mapValfrom0_180to0_255(105)// pin 11 pros ta exw
const rangeVal3 = servoCnrl.mapValfrom0_180to0_255(85) //pin 10 pros ta exw
const rangeVal4 = servoCnrl.mapValfrom0_180to0_255(56) // pin 10 pros ta mesa

const pinUnderBox = 9
const servoRangeUnderBox1 = servoCnrl.mapValfrom0_180to0_255(60)
const servoRangeUnderBox2 = servoCnrl.mapValfrom0_180to0_255(150)

const oddRange1_3 = [rangeVal1, rangeVal3]
const evenRange2_4 = [rangeVal2, rangeVal4]

const lastPosition = 0
const oneTimeRun = true;

module.exports = {
    stepsPerRev: stepsPerRev,
    stepperPins: stepperPins,
    servoTubePins: servoTubePins,
    allServoPins: allServoPins,
    rangeVal1: rangeVal1,
    rangeVal2:rangeVal2,
    rangeVal3:rangeVal3,
    rangeVal4:rangeVal4,
    oddRange1_3: oddRange1_3,
    evenRange2_4: evenRange2_4,
    servoRangeUnderBox1: servoRangeUnderBox1,
    servoRangeUnderBox2: servoRangeUnderBox2,
    pinUnderBox: pinUnderBox,
    lastPosition: lastPosition,
    oneTimeRun:oneTimeRun
}