var webpack = require('webpack')
var path = require('path')
var CommonsChunkPlugin = new require('webpack/lib/optimize/CommonsChunkPlugin')
var chunks = ['app']
var config = require('./package.json')

module.exports = {

	entry: {
		app: './src/index.js'
	},
	output: {
		path: __dirname + '/dist',
		filename: 'bundle/[name].js',
		sourceMapFilename: 'bundle/[name].map'
	},
	devtool: '#source-map',
	plugins: process.env.NODE_ENV === 'production' ? [
		new CommonsChunkPlugin({
			name: 'commons',
			chunks: chunks
		}),
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			minimize: true,
			compress: {
				warnings: false,
				drop_console: true
			}
		})
	] : [
			new CommonsChunkPlugin({
				name: 'commons',
				chunks: chunks
			})
		],
	module: {
		rules: [

			{
				test: /\.jsx?$/,

				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015']
				}

			},
			{
				test: /\.json$/,
				use: [
					{
						loader: 'json-loader'
					}
				]
			},
			{
				test: /\.(jpg|png|svg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'images/',
							publicPath: (config.server) ? '/' : 'dist/',
							useRelativePath: false
						}
					}
				]
			},
			{
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }

		]

	},
	
}

