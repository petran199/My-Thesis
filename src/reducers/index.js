import navReducer from './navReducer'
import sectionReducer from './sectionReducer'
import boardConfigReducer from './boardConfigReducer'

export {
	
	navReducer,
	sectionReducer,boardConfigReducer
	
}