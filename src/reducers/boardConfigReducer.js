import constants from '../constants'

var initialState = {
    stepperPins: [],
    servoTubePins: [],
    servoGatePin: null,
    arduinoPort: ""
}

export default (state = initialState, action) => {
    let newState = Object.assign({}, state)

    switch (action.type) {

        case constants.ARDUINO_PORT_ADDED:

            if (action.data) {
                newState['arduinoPort'] = action.data
                return newState
            }
        case constants.STEPPER_PINS_ADDED:

            if (action.data) {
                newState['stepperPins'] = action.data
                return newState
            }
        case constants.SERVO_TUBE_PINS_ADDED:

            if (action.data) {
                newState['servoTubePins'] = action.data
                return newState
            }
        case constants.SERVO_GATE_PIN_ADDED:

            if (action.data) {
                newState['servoGatePin'] = action.data
                return newState
            }

        default:
            return state
    }
}