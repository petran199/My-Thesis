import constants from '../constants'

var initialState = {
	items: [
		{ name: 'Dashboard', active: 'dashboard', link: '/home/dashboard' },
		{ name: 'Photos Library', active: 'photos', link: '/home/photos' },
		{ name: 'Videos Library', active: 'videos', link: '/home/videos' },
	]
}

export default (state = initialState, action) => {
	let newState = Object.assign({}, state)

	switch (action.type) {

		case constants.NAV_ITEMS_ADDED:

			if (action.data) {
				newState['items'].push(action.data)
				return newState
			}


		default:
			return state
	}
}