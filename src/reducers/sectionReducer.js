import constants from '../constants'
import React from 'react'

var initialState = {
    navMainMenuItems: [
        { href: "#color", name: 'Color options', className: "nav-link active" },
        { href: "#area", name: 'Area options', className: "nav-link " },
        { href: "#perimeter", name: 'Perimeter options', className: "nav-link " }
    ],
    NavMainMenuTabPanes: [
        { id: 'color', description: 'Color Options Tab Pane', active: 'active' },
        { id: 'area', description: 'Area Options Tab Pane', active: null },
        { id: 'perimeter', description: 'Perimeter Options Tab Pane', active: null }
    ],
    NavMainMenuTabPaneChildren: [
        [
            {
                child: <input className="form-control " style={{ width: 10 + "%", padding: 0 }} type="color" id="colorPicker" onInput={() => {
                    var inputText = document.getElementById('inputText')
                    var x = e.target.value.match(/[A-Za-z0-9]{2}/g).map(function (v) { return parseInt(v, 16) })
                    inputText.value = x
                }} />
            },
            { child: <input type="text" id="inputText" /> }
        ],
    ],

    trackingObjName: [],
    RGBinputVals: [],
    objNameRGBvalsMap: {},
    objNameStepperDistMap: {},
    ObjNamesArray: []
}

export default (state = initialState, action) => {
    let newState = Object.assign({}, state)

    switch (action.type) {

        case constants.NAV_MAIN_MENU_ITEMS_ADDED:
            newState['navMainMenuItems'].push(action.data)
            return newState
            
        case constants.NAV_MAIN_MENU_TAB_PANES_ADDED:
            newState['NavMainMenuTabPanes'].push(action.data)
            return newState

        case constants.NAV_MAIN_MENU_TAB_PANE_CHILDREN_ADDED:
            newState['NavMainMenuTabPaneChildren'].push(action.data)
            return newState

        case constants.TRACKING_OBJECT_NAME_ADDED:
            newState['trackingObjName'] = (action.data)
            return newState

        case constants.RGB_INPUT_VALS_ADDED:
            newState['RGBinputVals'] = action.data
            return newState

        case constants.RGB_STEPPER_DIST_OBJNAME_MAP_ADDED:
            newState['objNameRGBvalsMap'] = action.objNameRGBvalsMap
            newState['objNameStepperDistMap'] = action.objNameStepperDistMap
            newState['ObjNamesArray'] = action.ObjNamesArray
            return newState

        default:
            return state
    }
}