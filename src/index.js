import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import store from './stores'
import { Provider } from 'react-redux'
import { Home, LoginForm } from './components/layout'
import { PageNotFound } from './components/presentation'
import { Router, Route, Link, Switch, Redirect } from 'react-router-dom'
import { history } from './utils'

// Check the route path and redirect properly
const checkPath = ({ match }) => {

	var matched = false
	const path = 'home'
	const dash = 'dashboard'
	const login = 'login'

	if (path == match.params.path) {
		matched = true
		window.currentPath = dash
		return null
	} else if (login == match.params.path && !window.logedIn) {
		matched = true
		window.currentPath = login
		return null
	} else {
		window.currentPath = dash
		return <PageNotFound />
	}
}

window.logedIn = true
const app = (
	<Provider store={store.configure(null)}>
		<Router history={history} >
			<div style={{ height: '100% !important' }} >
				<Route exact path="/" render={() => window.logedIn ? <Redirect to="/home/" /> : <Redirect to="/login/" />} />
				<Route path="/:path" render={checkPath} />
				<Route path="/home/" component={Home} />
				<Route path="/login/" render={() => window.logedIn ? <Redirect to="/home/" /> : <LoginForm />} />
			</div>
		</Router>
	</Provider>
)

ReactDOM.render(app, document.getElementById('root'))
