
export default {

	USERS_RECEIVED: 'USERS_RECEIVED',
	USER_CREATED: 'USER_CREATED',
	USER_LOGGED_IN: 'USER_LOGGED_IN',
	CURRENT_USER_RECEIVED: 'CURRENT_USER_RECEIVED',
	NAV_ITEMS_ADDED: 'NAV_ITEMS_ADDED',
	NAV_MAIN_MENU_ITEMS_ADDED: 'NAV_MAIN_MENU_ITEMS_ADDED',
	NAV_MAIN_MENU_TAB_PANES_ADDED: 'NAV_MAIN_MENU_TAB_PANES_ADDED',
	NAV_MAIN_MENU_TAB_PANE_CHILDREN_ADDED: 'NAV_MAIN_MENU_TAB_PANE_CHILDREN_ADDED',
	TRACKING_OBJECT_NAME_ADDED: 'TRACKING_OBJECT_NAME_ADDED',
	RGB_INPUT_VALS_ADDED: 'RGB_INPUT_VALS_ADDED',
	STEPPER_PINS_ADDED: 'STEPPER_PINS_ADDED',
	SERVO_TUBE_PINS_ADDED: 'SERVO_TUBE_PINS_ADDED',
	SERVO_GATE_PIN_ADDED: 'SERVO_GATE_PIN_ADDED',
	ARDUINO_PORT_ADDED: 'ARDUINO_PORT_ADDED',
	RGB_STEPPER_DIST_OBJNAME_MAP_ADDED:'RGB_STEPPER_DIST_OBJNAME_MAP_ADDED'



}