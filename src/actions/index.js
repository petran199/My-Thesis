import constants from '../constants'

export default {
	
	// Add obj names with its RGB vals, the names of the objects, the obj name with the distance of the stepper that is used, to the store
	addMapsToStore: (objNameRGBvalsMap,ObjNamesArray,objNameStepperDistMap) =>{
		return dispatch =>{
			dispatch({
				type: constants.RGB_STEPPER_DIST_OBJNAME_MAP_ADDED,
				objNameRGBvalsMap: objNameRGBvalsMap,
				ObjNamesArray: ObjNamesArray,
				objNameStepperDistMap: objNameStepperDistMap
			})
		}
	},

    // Store the Pins of the steppers we want to use 
	addStepperPins: (stepperPins) => {

		var parseIntOnStepperPins = stepperPins.split(',').map( stepperPin=> {
			return parseInt(stepperPin, 10);
		});
		stepperPins = parseIntOnStepperPins
		return dispatch =>{
			dispatch({
				type: constants.STEPPER_PINS_ADDED,
				data:[...stepperPins]
			})
		}
	},
	
	// Store the servo motor pins that are attached on Tube construction
	addServoTubePins: (servoTubePins) => {

		var parseIntOnServoTubePins = servoTubePins.split(',').map( servoTubePin=> {
			return parseInt(servoTubePin, 10);
		});
		servoTubePins = parseIntOnServoTubePins
		return dispatch => {
			dispatch({
				type: constants.SERVO_TUBE_PINS_ADDED,
				data:[...servoTubePins]
				
			})
		}
		

	},
	
	// Store the servo pin that is responsible for letting the obj go when it's recognised	
	addServoGatePin: (servoGatePin) => {
		
		var servoGatePinToInt = parseInt(servoGatePin,10)
		servoGatePin = servoGatePinToInt
		return dispatch => {
			dispatch({
				type: constants.SERVO_GATE_PIN_ADDED,
				data: servoGatePin
			})
		}
	},
	
	// Store the Arduino USB port
	addArduinoPort: (arduinoPort) =>{

		if(!typeof(arduinoPort)=="string"){
			var arduinoPortToString = arduinoPort.toString()
			arduinoPort = arduinoPortToString
		}
		return dispatch => {
			dispatch({
				type: constants.ARDUINO_PORT_ADDED,
				data: arduinoPort
			})
		}

	},
	
	// Store the navigation menu items 
	AddNavItems: (params) => {
		return dispatch => {
			dispatch({
				type: constants.NAV_ITEMS_ADDED,
				data: params
			})
			
		}
	},
	
	// Store the the object name we wanna track
	addTrackingObjName:(params)=>{
		return(dispatch) => {
			dispatch({
				type:constants.TRACKING_OBJECT_NAME_ADDED,
				data:params
			})
		}
	},

	// Store user's RGB input vals
	addRGBinputVals:(rgbInputVals)=>{
		return(dispatch) => {
			dispatch({
				type:constants.RGB_INPUT_VALS_ADDED,
				data:[...rgbInputVals]
			})
		}
	},
}
