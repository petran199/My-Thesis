// Check if user didn't leave any RGB input field empty
var RGBinputsNotEmpty = () => {

    let counter = 0
    var rgbInputIds = ['rGT', 'rLT', 'gGT', 'gLT', 'bGT', 'bLT']
    var rgbInputVals = rgbInpVals(rgbInputIds)

    rgbInputVals.forEach(val => {
        counter = (val.value != "") ? counter += 1 : counter
    })

    if (counter == rgbInputVals.length) {
        return true
    }

    return false
}

// Check if user didn't leave the object name field empty
var objectNameNotEmpty = () => {

    let objName = document.getElementById('objectName')

    if (objName.value != "") {
        return true
    } else {
        return false
    }
}

// handle appropriately the target id of drop down menu on Color options tab on Dashboard navigation menu
var swOnDropDownID = (targetID) => {

    if (RGBinputsNotEmpty()) {

        switch (targetID) {
            case 'o5':
                handleDropDownOffset(5)
                break
            case 'o10':
                handleDropDownOffset(10)
                break
            case 'o15':
                handleDropDownOffset(15)
                break
            default:
                console.log("error on handle click at offset dropdown")
                break
        }

    } else { alert("RGB Input fields are Null") }
}

// handle the offset of RGB vals on input fields, depending on the option that user has selected on the offset dropdown menu
var handleDropDownOffset = (mult) => {

    var rgbInputIds = ['rGT', 'rLT', 'gGT', 'gLT', 'bGT', 'bLT']
    var rgbInputVals = rgbInpVals(rgbInputIds)

    rgbInputVals.forEach((val, index) => {

        if (val.value >= mult && val.value <= (255 - mult)) {

            if (index % 2 == 0) {
                let x = parseInt(val.value)
                x = x + mult
                val.value = x
            } else {
                let x = parseInt(val.value)
                x = x - mult
                val.value = x
            }

        } else if (val.value < mult) {

            if (index % 2 == 0) {
                let x = parseInt(val.value)
                x = x + mult
                val.value = x
            } else {
                val.value = 0
            }

        } else {

            if (index % 2 == 0) {
                val.value = 255
            } else {
                let x = parseInt(val.value)
                x = x - mult
                val.value = x
            }
        }
    })
}

// store the current values of the rgb input fields to an Array
var rgbInpVals = (args) => {

    var y = []

    args.forEach(val => {
        y.push(document.getElementById(val))
    })
    return y
}

// reset the content of al input fields on Color options
var resetContentVals = () => {

    var rgbInputIds = ['rGT', 'rLT', 'gGT', 'gLT', 'bGT', 'bLT', 'objectName']
    var rgbInputVals = rgbInpVals(rgbInputIds)

    rgbInputVals.forEach(val => {
        val.value = null
    })
}

// get the input values of rgb input fields that is located on Color options
var getInputVals = () => {

    var rgbInputIds = ['rGT', 'rLT', 'gGT', 'gLT', 'bGT', 'bLT']
    var rgbInputVals = rgbInpVals(rgbInputIds)
    let inputValsArray = rgbInputVals.map(val => {
        return val.value
    })
    return inputValsArray
}

// Apply the RGB values of the color pick element to rgb input fields
var colorPick = (e) => {

    var rgbInputIds = ['rGT', 'rLT', 'gGT', 'gLT', 'bGT', 'bLT']
    var rgbInputVals = rgbInpVals(rgbInputIds)
    var x = e.target.value.match(/[A-Za-z0-9]{2}/g).map(function (v) { return parseInt(v, 16) })

    rgbInputVals.forEach((val, i) => {

        if (i < 2) {
            val.value = x[0]
        } else if (i >= 2 && i < 4) {
            val.value = x[1]
        } else {
            val.value = x[2]
        }
    })
}

// Set the RGB vals to the respective RGB input fields when the user click on video element
var setRGBValsOnCanvasClick = (r, g, b) => {

    var rgbInputIds = ['rGT', 'rLT', 'gGT', 'gLT', 'bGT', 'bLT']
    var rgbInputVals = rgbInpVals(rgbInputIds)

    rgbInputVals.forEach((val, i) => {

        if (i < 2) {
            val.value = r
        } else if (i >= 2 && i < 4) {
            val.value = g
        } else {
            val.value = b
        }
    })
}

export default {
    handleDropDownOffset,
    rgbInpVals,
    swOnDropDownID,
    resetContentVals,
    colorPick,
    RGBinputsNotEmpty,
    objectNameNotEmpty,
    getInputVals,
    setRGBValsOnCanvasClick
}