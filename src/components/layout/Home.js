import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavMenu, SectionField, LoginMenu } from '../containers'
import { Header } from '../presentation'
import { Route, Redirect } from 'react-router-dom'
import { Photos, Videos } from '../presentation/Section'
import bg_img from '../../../assets/images/pattern.png'



class Home extends Component {
    constructor() {
        super()
        this.state = {
            handlingRoutes: [
                { path: "/home/", render: () => <Redirect to="/home/dashboard" /> },
                { path: "/home/:path", render: this.checkPath.bind(this) },
                { path: "/home/:path/*", render: () => <Redirect to="/home/dashboard" /> },
            ],
            componentRoutes: [
                { path: "/home/dashboard", component: SectionField },
                { path: "/home/videos", component: Videos },
                { path: "/home/photos", component: Photos },
            ]
        }
    }

    checkPath({ match }) {

        var matched = false

        const paths = this.props.nav.items.map((item, i) => {
            return (
                item.active
            )
        })

        paths.some(path => {

            if (path == match.params.path) {
                window.currentPath = match.params.path
                matched = true
            }
            return matched == true
        })

        if (!matched) {
            window.currentPath = paths[0] /* this is 'home'*/
            return <Redirect to={'/' + match.params.path} />
        }
        return null
    }

    render() {

        let handlingRoutes = this.state.handlingRoutes.map(({ path, render }, key) =>
            <Route key={key} exact path={path} render={render} />
        )

        let componentRoutes = this.state.componentRoutes.map(({ path, component }, key) =>
            <Route key={key} exact path={path} component={component} />
        )

        return (

            <div className="container-fluid" style={style.parrent} >
                {handlingRoutes}

                <div className="row" style={style.rowOne} >

                    <div style={style.header} className="col-xs-2 col-sm-2 col-md-2">
                        {/* <Header /> */}
                    </div>

                    <div style={style.loginMenu} className="col-xs-10 col-sm-10 col-md-10">

                        <LoginMenu />
                    </div>

                </div>

                <div className="row " style={style.rowTwo} >

                    <div style={style.navMenu} className="col-xs-2 col-sm-2 col-md-2" >
                        <NavMenu />
                    </div>

                    <div className="col-xs-10 col-sm-10 col-md-10" style={style.components}  >
                        {componentRoutes}
                    </div>

                </div>
            </div>
        )
    }

}

const stateToProps = (state) => {
    return {
        nav: state.nav
    }
}

const dispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(stateToProps, dispatchToProps)(Home)

const style = {
    parrent: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        height: '100%'
    },
    header: {
        height: '100%',
        backgroundColor: '#dbabab'
    },
    loginMenu: {
        backgroundColor: '#98bda9',
        height: '100%'
    },
    navMenu: {
        backgroundColor: '#97b0d8',
        height: '100%'
    },
    components: {
        height: '100%',
        overflow: 'auto',
        backgroundImage: `url(${bg_img})`
    },
    footer: {
    },
    rowTwo: {
        height: '90%',
    },
    rowOne: {
        height: '10%',
    }
}