import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import { Form, Button } from 'semantic-ui-react'


class LoginForm extends Component {
    constructor() {
        super()
        this.state = {

            user: "",
            pass: "",
            submittedPass: "",
            submittedUser: ""
        }

        // save user's user and pass
        this.handleChange = (e, { name, value }) => this.setState({ [name]: value })

        this.handleSubmit = (e) => {
            e.preventDefault()
            const { user, pass } = this.state
            this.setState({ submittedPass: pass, submittedUser: user })
        }
    }

    render() {

        var { handleChange, handleSubmit, user, pass } = this.state
        return (
            <div>
                <Route path="/login/*" render={() => window.logedIn ? <Redirect to="/home/" /> : <Redirect to="/login/" />} />
                < Form >
                    <Form.Group unstackable >
                        <Form.Input label='Username' placeholder='petchatz' name="user" value={user} onChange={this.handleChange} />
                    </Form.Group>

                    <Form.Group >
                        <Form.Input label='Password' placeholder='1' name="pass" value={pass} />
                    </Form.Group>

                    <Form.Checkbox label='I agree to the Terms and Conditions' />

                    <Button onClick={this.handleSubmit} >Login</Button>
                </Form >
            </div >
        )
    }
}

const styles = {

}

const stateToProps = (state) => {
    return {

    }
}

const dispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(stateToProps, dispatchToProps)(LoginForm)
