import React, { Component } from 'react'

export default (props) => {

    return (

        <div style={{ position: 'absolute', top: 0, left: 0, right: 0, height: '100%' }} >

            <iframe style={{ width: '100%', display: 'block', border: 'none', padding: 50 }} src="https://drive.google.com/file/d/185n4Ic31LoA_9qAT_6yCXg9dNr-HXxzi/preview" width="640" height="480" allowFullScreen></iframe>

        </div>
    )
}