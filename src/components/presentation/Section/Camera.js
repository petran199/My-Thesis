import React, { Component } from 'react'


export default (props) => {
    return (
        <div >

            <div className="row " style={{ padding: '20px 15px 20px 15px' }} >
                <div >
                    <label htmlFor="videoSource">Video source: </label>
                </div>
                <div style={{ overflow: 'hidden' }} >
                    <select onChange={props.vidSourceOnChange} id="videoSource"></select>
                </div>
            </div>

            <div className="row " style={{ padding: '0px 15px 20px 15px' }} >
                <div className="    embed-responsive embed-responsive-4by3" >
                    <video id="video" className="embed-responsive-item" ref={props.videoRef} preload="true" autoPlay loop muted></video>
                    <canvas id="canvas" className="embed-responsive-item" ref={props.canvasRef}  ></canvas>
                </div>

            </div>
        </div>
    )
}


const style = {
    testCanvas: {
        border: "2px solid black"
    }
}