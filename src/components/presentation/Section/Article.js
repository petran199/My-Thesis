import React, { Component } from 'react'
import TabPane from './TabPane'

export default (props) => {
  return (

    <div>
      <TabPane colorPick={props.colorPick} addObjectName={props.addObjectName} handleOffset={props.handleOffset} addContentVals={props.addContentVals} resetContentVals={props.resetContentVals} />
    </div>
  )
}