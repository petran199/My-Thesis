import React, { Component } from 'react'
import { Input, Label, Button, } from 'semantic-ui-react'

export default (props) => {

    return (
        <div className="tab-pane " id="area" role="tabpanel" >

            <h3>Area Options</h3>

            <Input size="mini" labelPosition='right' type='text'>
                <input id="areaGT" placeholder='Area Greater Than' />
                <Label basic> > Area ></Label>
                <input id="areaLT" placeholder='Area Less Than' />
            </Input>
            
            <br /><br />
            <Button circular icon='checkmark' />
        </div>
    )
}


