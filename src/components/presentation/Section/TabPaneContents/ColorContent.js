import React, { Component } from 'react'
import { Input, Label, Button, Dropdown, Grid, Menu, Popup } from 'semantic-ui-react'

export default (props) => {
    const colSize_3 = "col-xs-3 col-sm-3 col-md-3"
    return (
        <div >

            <div className="tab-pane active " id="color" role="tabpanel">

                <h3>Color Options</h3>

                <div className="row ">
                    <div className={colSize_3} >
                        <Label style={{ width: "100%", marginBottom: 5, overflow: 'auto' }} basic> Pick a color       </Label>
                    </div>

                    <div className={colSize_3} >
                        <Popup
                            trigger={<Input size='mini' style={{ width: "100%" }} type="color" id="colorPicker" onInput={props.colorPick} />}
                            size="mini"
                            content="click to pick your color"
                            position='top center'
                            basic
                        />
                    </div>
                </div>

                <div className="row ">
                    <div className={colSize_3} >
                        <Label style={{ width: "100%", overflow: 'auto' }} basic> Write object name </Label>
                    </div>

                    <div className={colSize_3}  >
                        <Input style={{ width: '100%' }} size='mini' id="objectName" onChange={props.addObjectName} placeholder='Object name' />

                    </div>

                    <div className={colSize_3} >
                        <Menu compact size='mini' >
                            <Dropdown text='Offset' simple item>
                                <Dropdown.Menu>
                                    <Dropdown.Item id="o5" text='+5' onClick={props.handleOffset} />
                                    <Dropdown.Item id="o10" text='+10' onClick={props.handleOffset} />
                                    <Dropdown.Item id="o15" text='+15' onClick={props.handleOffset} />
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu>

                    </div>
                </div>

                <br />
                <Grid container={true} >
                    <h4 style={{ marginBottom: 0, marginTop: 40 }}  >Input Values</h4>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Input size='mini' labelPosition='right' type='text' >
                                <input id="rGT" placeholder='Red Greater Than' />
                                <Label basic> > R > </Label>
                                <input id="rLT" placeholder='Red Less Than' />
                            </Input>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Input size="mini" labelPosition='right' type='text'>
                                <input id="gGT" placeholder='Green Greater Than' />
                                <Label basic> > G ></Label>
                                <input id="gLT" placeholder='Green Less Than' />
                            </Input>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Input size='mini' labelPosition='right' type='text'>
                                <input id="bGT" placeholder='Blue Great Than' />
                                <Label basic> > B ></Label>
                                <input id="bLT" placeholder='Blue Less Than' />
                            </Input>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>

                <br />
                <Popup
                    trigger={<Button id='colorContentAddBtn' onClick={props.addContentVals} circular icon='checkmark' />}
                    size="mini"
                    content="Apply changes"
                    position='bottom center'
                    basic
                />
                
                <Popup
                    trigger={<Button id='colorContentResetBtn' onClick={props.resetContentVals} circular icon='undo' />}
                    size="mini"
                    content="Reset all input fields"
                    position='bottom center'
                    basic
                />
            </div>
        </div>
    )
}


const style = {
    colorPicker: {
        width: "80%",
        padding: 0
    },
    objectName: {
        width: "95%"
    }
}