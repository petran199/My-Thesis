import React, { Component } from 'react'
import { Input, Label, Button, } from 'semantic-ui-react'

export default (props) => {

    return (
        <div className="tab-pane " id="perimeter" role="tabpanel">

            <h3> Perimeter Options</h3>

            <Input size="mini" labelPosition='right' type='text'>
                <input id="perimeterGT" placeholder='Perimeter Greater Than' />
                <Label basic> > Perimeter ></Label>
                <input id="perimeterLT" placeholder='Perimeter Less Than' />
            </Input>
            
            <br /><br />
            <Button circular icon='checkmark' />
        </div>
    )
}
