import AreaContent from './AreaContent'
import PerimeterContent from './PerimeterContent'
import ColorContent from './ColorContent'


export {
    
    AreaContent,
    PerimeterContent,
    ColorContent,
}