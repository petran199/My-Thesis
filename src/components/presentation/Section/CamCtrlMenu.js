import React, { Component } from 'react'
import { Button, Popup } from 'semantic-ui-react'

export default (props) => {

    return (
        <div>
            <Button.Group>
                <Popup
                    trigger={<Button icon='record' id='startBtn' onClick={props.handleStartBtn} />}
                    size="mini"
                    content="Start Camera"
                    position='bottom center'
                    basic
                />
                <Popup
                    trigger={<Button icon='stop' onClick={props.handleStopBtn} id='stopBtn' />}
                    size="mini"
                    content="Stop Camera"
                    position='bottom center'
                    basic
                />
                <Popup
                    trigger={<Button icon='pause' onClick={() => { document.getElementById('video').pause() }} />}
                    size="mini"
                    content="Pause the frames"
                    position='bottom center'
                    basic
                />
                <Popup
                    trigger={<Button icon='play' onClick={() => { document.getElementById('video').play() }} />}
                    size="mini"
                    content="Continue running the frames"
                    position='bottom center'
                    basic
                />
                <Popup
                    trigger={<Button icon='window maximize' onClick={() => { document.getElementById('video').webkitRequestFullscreen() }} />}
                    size="mini"
                    content="Maximaze camera"
                    position='bottom center'
                    basic
                />

            </Button.Group>
        </div>
    )
}