import React, { Component } from 'react'

export default (props) => {

    return (
        <div style={{ position: 'absolute', top: 0, left: 0, right: 0, height: '100%' }} >
            <iframe style={{ width: '100%', height: '100%', display: 'block', border: 'none' }} src="https://drive.google.com/embeddedfolderview?id=1KZ30ykXIphLghzKSvqhtImfs1AAlBKSs#grid" frameBorder="0" ></iframe>
        </div>
    )
}
