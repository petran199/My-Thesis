import React, { Component } from 'react'
import { Header, Article, Footer, } from './index'

export default (props) => {
    return (
        <div>

            <div>
                <Article colorPick={props.colorPick} addObjectName={props.addObjectName} handleOffset={props.handleOffset} addContentVals={props.addContentVals} resetContentVals={props.resetContentVals} />
            </div>

        </div >
    )
}