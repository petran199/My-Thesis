import React, { Component } from 'react'
import { AreaContent, ColorContent, PerimeterContent } from './TabPaneContents'

export default (props) => {
    const tabPane = {
        items: [
            { className: "nav-item active", classNameAnchor: "nav-link ", href: "#color", name: "Color Options" },
            // { className: "nav-item", classNameAnchor: "nav-link ", href: "#area", name: "Area Options" },
            // { className: "nav-item", classNameAnchor: "nav-link ", href: "#perimeter", name: "Perimeter Options" },
        ],

    }

    const tabPaneItems = tabPane.items.map((item, i) => {
        return (
            <li key={i} className={item.className}>
                <a key={i} className={item.classNameAnchor} data-toggle="tab" href={item.href} role="tab" >{item.name}</a>
            </li>
        )
    })


    return (
        <div >
            <ul className="nav nav-tabs" role="tablist">
                {tabPaneItems}
            </ul>

            <div className="tab-content">
                {/***** 1st tab pane  ******/}
                <div className="tab-pane active " id="color" role="tabpanel">
                    <ColorContent colorPick={props.colorPick} addObjectName={props.addObjectName} handleOffset={props.handleOffset} addContentVals={props.addContentVals} resetContentVals={props.resetContentVals} />
                </div>
                {/***** 2nd tab pane  ******/}
                {/* <div className="tab-pane " id="area" role="tabpanel">
                    <AreaContent />
                </div> */}
                {/***** 3rd tab pane  ******/}
                {/* <div className="tab-pane " id="perimeter" role="tabpanel">
                    <PerimeterContent />
                </div> */}

            </div>
        </div>
    )
}


