import Nav from './Nav'
import Header from './Header'
import Footer from './Footer'
import PageNotFound from './PageNotFound'
import ConfigSegment from './ConfigSegment'
import ListObjectInfo from './ListObjectInfo'

export {
        
    Nav,
    Header,
    Footer,
    PageNotFound,
    ConfigSegment,
    ListObjectInfo
}