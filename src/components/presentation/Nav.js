import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

export default (props) => {

  const nav = props.items.map((item, i) => {
    return (
      <Menu.Item id={item.active} name={item.name} key={i} as={Link} to={item.link} active={props.activeMenuItem === item.active} onClick={props.handleMenuItemClick} />
    )
  })


  return (

    <div style={{ position: 'absolute', left: 0, width: '100%' }}>
      <Menu style={{ width: 'auto' }} pointing secondary vertical>
        {nav}
      </Menu>
    </div>
  )
}