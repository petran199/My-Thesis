import React, { Component } from 'react'
import { Image } from 'semantic-ui-react'
import teikav from '../../../assets/images/teikav.png'

export default (props) => {

    return(
        <div>
            <Image src={teikav} size='mini' centered  style={{width:60}}/>
       </div>
    )

}