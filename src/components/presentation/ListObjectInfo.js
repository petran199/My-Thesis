import React, { Component } from 'react'
import { Icon, List } from 'semantic-ui-react'

export default (props) => {

    const listObjInfo = props.listObjInfo.map((objInfo, i) => {
        return (
            <List.Item key={i} as='a' >
                <Icon name='remove' />
                <List.Content /* style={{overflow:"auto"}} */>
                    <List.Header>{objInfo.header}</List.Header>
                    <List.Description style={{ fontSize: "90%" }} >
                        RedMax: {objInfo.redMax} | RedMin: {objInfo.redMin} | GreenMax: {objInfo.greenMax} | GreenMin: {objInfo.greenMin} | BlueMax: {objInfo.blueMax} | BlueMin: {objInfo.blueMin}

                    </List.Description>
                </List.Content>
            </List.Item>
        )
    })

    // Return the list of the objects that the user has register for the tracking process
    return (
        <List celled   >
            {listObjInfo}
        </List>

    )

}
