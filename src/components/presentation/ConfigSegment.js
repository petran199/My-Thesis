// The configuration segment menu to set your desired configuration to the arduino board
import React from 'react'
import { Segment, Form, Label } from 'semantic-ui-react'

const ConfigSegment = (props) => (
    <div>
        <Form>
            <Segment vertical>
                <h4>Arduino Uno board configuration</h4>
                <Form.Field inline>
                    <div>Provide the port name of you connected arduino Uno device.</div><br />
                    <Label pointing='right'>Arduino port</Label>
                    <input type='text' placeholder='/dev/ttyUSB0' onChange={props.addArduinoPort} />
                </Form.Field>
            </Segment>

            <Segment vertical>
                <h4>Stepper motor configuration</h4>
                <Form.Field inline>
                    <div>Provide the pins you used for connecting your stepper motor on arduino Uno board.</div><br />
                    <Label pointing='right'>Stepper pins </Label>
                    <input type='text' placeholder='5,7,6,8' onChange={props.addStepperPins} />
                </Form.Field>
            </Segment>

            <Segment vertical>
                <h4>Servo tube construction motors configuration</h4>
                <Form.Field inline>
                    <div>Provide the pins you used for connecting your servo tube construction motors, on arduino Uno board.</div><br />
                    <Label pointing='right'>Servo tube pins </Label>
                    <input type='text' placeholder='11,10' onChange={props.addServoTubePins} />
                </Form.Field>
            </Segment>
            
            <Segment vertical>
                <h4>Servo gate motor configuration</h4>
                <Form.Field inline>
                    <div>Provide the pin you used for connecting your servo motor, that is used as a gate for the recognised objects, on arduino Uno board.</div><br />
                    <Label pointing='right'>Servo gate pin </Label>
                    <input type='text' placeholder='9' onChange={props.addServoGatePin} />
                </Form.Field>
            </Segment>
        </Form>
    </div>
)

export default ConfigSegment
