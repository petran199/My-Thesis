import React, { ReactComponent } from 'react'
import { history } from '../../utils'


export default (props) => {

    return (
        <div>
            <h3>
                Page Not Found !!!
            </h3>
            <div>
                You are redirecting ...
            </div>

            {
                setTimeout(() => {
                    if (window.logedIn) {
                        history.push('/home/')
                    } else {
                        history.push('/login/')
                    }
                }, 3000)
            }
        </div>
    )

}
