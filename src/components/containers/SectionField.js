import React, { Component } from 'react'
import { connect } from 'react-redux'
import actions from '../../actions'
import { Section, Header } from '../presentation/Section'
import { ListObjectInfo } from '../presentation'
import { ColorTabPane } from '../../utils'
import { CameraField } from './index'
require('tracking')

class SectionField extends Component {
    constructor() {
        super()
        this.state = {
            trackingObjName: null,
            objNameRGBvalsMap: {},
            objNameStepperDistMap: {},
            stepperDistBetweenCarts: 2000,
            trackingObjNamesArray: [],
            listObjInfo: []
        }
        ColorTabPane.colorPick = ColorTabPane.colorPick.bind(this)
        this.handleOffset = this.handleOffset.bind(this)
        this.addContentVals = this.addContentVals.bind(this)
        this.addObjectName = this.addObjectName.bind(this)
        ColorTabPane.resetContentVals = ColorTabPane.resetContentVals.bind(this)
    }

    componentDidMount() {

    }
    componentWillUnmount() {


    }

    // save object name to state
    addObjectName(e) {

        let trackingObjName = Object.assign({}, this.state.trackingObjName)
        trackingObjName = e.target.value

        this.setState({
            trackingObjName: trackingObjName
        })
    }

    // Save state's object name to store
    addTrackingObjName() {
        this.props.addTrackingObjName(this.state.trackingObjName)
    }

    // Use the tracking.js lib to register the desired color for the tracking process
    registerColor() {

        var rgbInputVals = ColorTabPane.getInputVals()
        this.props.addRGBinputVals(rgbInputVals)

        tracking.ColorTracker.registerColor(this.state.trackingObjName, function (r, g, b) {

            if (r >= rgbInputVals[1] && r <= rgbInputVals[0] && g >= rgbInputVals[3] && g <= rgbInputVals[2] && b >= rgbInputVals[5] && b <= rgbInputVals[4]) {
                return true;
            }

            return false;
        })
    }

    // Add  object names of tracking objects, their RGB values and the distance between the carts on the bottom of the box, to the store
    storeObjNamesRGBvalsCartDist() {

        var rgbInputVals = ColorTabPane.getInputVals()
        let objNameRGBvalsMap = Object.assign({}, this.state.objNameRGBvalsMap)
        objNameRGBvalsMap[this.state.trackingObjName] = rgbInputVals

        let trackingObjNamesArray = Object.assign([], this.state.trackingObjNamesArray)
        trackingObjNamesArray.push(this.state.trackingObjName)

        let objNameStepperDistMap = Object.assign({}, this.state.objNameStepperDistMap)
        trackingObjNamesArray.map((objName, i) => {
            objNameStepperDistMap[objName] = this.state.stepperDistBetweenCarts * (i + 1)
        })
        this.props.addMapsToStore(objNameRGBvalsMap, trackingObjNamesArray, objNameStepperDistMap)

        let objInfoDir = {}
        objInfoDir.header = this.state.trackingObjName
        objInfoDir.redMax = rgbInputVals[0]
        objInfoDir.redMin = rgbInputVals[1]
        objInfoDir.greenMax = rgbInputVals[2]
        objInfoDir.greenMin = rgbInputVals[3]
        objInfoDir.blueMax = rgbInputVals[4]
        objInfoDir.blueMin = rgbInputVals[5]
        let listObjInfo = Object.assign([], this.state.listObjInfo)
        listObjInfo.push(objInfoDir)

        this.setState({
            objNameRGBvalsMap: objNameRGBvalsMap,
            trackingObjNamesArray: trackingObjNamesArray,
            objNameStepperDistMap: objNameStepperDistMap,
            listObjInfo: listObjInfo
        })
    }

    // Add the object name to state, store the objname, its vals and the distance between carts and then register the colort via tracking.js lib, finally reset the vals of the input fields
    addContentVals(e) {

        e.preventDefault()

        if (ColorTabPane.RGBinputsNotEmpty() && ColorTabPane.objectNameNotEmpty()) {

            this.addTrackingObjName()
            this.storeObjNamesRGBvalsCartDist()
            this.registerColor()
            ColorTabPane.resetContentVals()

        } else {
            alert("Input Fields are null")
        }
    }

    // handle the offset of the dropdown menu
    handleOffset(e) {

        e.preventDefault()
        ColorTabPane.swOnDropDownID(e.currentTarget.id)
    }

    render() {
        var { listObjInfo } = this.state
        return (
            <div style={{ height: '100%' }} >
                <Header />
                <div className="row" style={{ height: '100%' }} >
                    <div className="col-md-7 col-sm-7">
                        <Section colorPick={ColorTabPane.colorPick} handleOffset={this.handleOffset} addObjectName={this.addObjectName} resetContentVals={
                            ColorTabPane.resetContentVals} addContentVals={this.addContentVals} />
                    </div>

                    <div className=" col-md-5 col-sm-5">
                        <CameraField />
                    </div>

                    <div className=" col-md-12 col-sm-12" style={{ margin: "50px 0 100px 0", overflow: 'auto' }} >
                        <h3>Object list information</h3>
                        <ListObjectInfo listObjInfo={listObjInfo} />
                    </div>
                </div>
            </div>
        )
    }
}

const stateToProps = (state) => {
    return {
        section: state.section
    }
}

const dispatchToProps = (dispatch) => {
    return {
        addTrackingObjName: (params) => dispatch(actions.addTrackingObjName(params)),
        addRGBinputVals: (rgbInputVals) => dispatch(actions.addRGBinputVals(rgbInputVals)),
        addMapsToStore: (objNameRGBvalsMap, ObjNamesArray, objNameStepperDistMap) => dispatch(actions.addMapsToStore(objNameRGBvalsMap, ObjNamesArray, objNameStepperDistMap))
    }
}

export default connect(stateToProps, dispatchToProps)(SectionField)
