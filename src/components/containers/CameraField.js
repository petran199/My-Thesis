import React, { Component } from 'react'
import { connect } from 'react-redux'
import io from 'socket.io-client'
import { Camera, CamCtrlMenu } from '../presentation/Section'
import { ColorTabPane } from '../../utils'
require('tracking')

class CameraField extends Component {
    constructor() {
        super()

        this.state = {

            isRuning: false,
            cntr: 0,
        }

        this.handleStartBtn = this.handleStartBtn.bind(this)
    }

    componentDidMount() {

        this.socket = io(`localhost:3000`, {
            transports: ['websocket'],
            upgrade: false,
        })

        // get the color while user click on video element (actually its canvas element)
        $('#canvas').click(function (e) {

            function getMousePos(canvas, evt) {
                var rect = canvas.getBoundingClientRect();
                return {
                    x: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width,
                    y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
                };
            }

            var video = document.getElementById('video')
            var canvas = this.getContext('2d')
            var mousePos = getMousePos(this, e)
            canvas.drawImage(video, 0, 0, video.clientWidth, video.clientHeight)
            var p = canvas.getImageData(mousePos.x, mousePos.y, 1, 1).data
            ColorTabPane.setRGBValsOnCanvasClick(p[0], p[1], p[2])
        })
    }

    // track the colors based on previous user's color registration
    trackColors() {

        this.colors = new tracking.ColorTracker(this.props.section.ObjNamesArray)

        this.colors.on('track', event => {

            let canvas = this.canvasRefs
            let context = canvas.getContext('2d')
            context.clearRect(0, 0, canvas.width, canvas.height)

            if (event.data.length === 0) {
                this.negRespToService()
            }
            else {
                this.handleFoundFruits(event, context)
            }
        })

        tracking.track(this.cameraRefs, this.colors, { camera: true })
    }

    // comunnicate with backend and handle the motors properly when the object (fruit) has been recognised
    handleFoundFruits(event, context) {

        this.setState((prevState) => ({
            cntr: 0,
        }))

        var self = this

        event.data.forEach(rect => {

            context.font = '14px Helvetica'
            context.fillStyle = "#ffffff"
            context.fillText(rect.color, rect.x + 5, rect.y + 15)

            self.props.section.ObjNamesArray.forEach((objName, i) => {

                if (rect.color == objName) {
                    self.fruitFunc(self, objName, self.props.section.objNameStepperDistMap[objName])
                }

                if (rect.color != objName && i == self.props.section.ObjNamesArray.length) {
                    console.log("something passed withoud declared in registerColor")
                }
            })

            context.strokeStyle = '#a64ceb'
            context.strokeRect(rect.x, rect.y, rect.width, rect.height)
        })
    }

    // comunicate with backend when the fruit has been found
    fruitFunc(self, fruitName, stepperMoveDistance) {

        if (!self.state.isRuning) {

            let isRuning = !self.state.isRuning

            self.setState({
                isRuning: isRuning
            })

            self.socket.emit("fruitFound", fruitName, stepperMoveDistance, (cb) => {

                console.log('cb ' + fruitName + ' from  server: ' + cb)

                setTimeout(() => {
                    console.log('cb  from  server: ')
                    self.setState({
                        isRuning: false
                    })

                }, 1000)
            })
        }
    }

    // if nothing has been found within some seconds then comunicate with backend to let the tube motors bring the next item to be recognised
    negRespToService() {

        // if (!this.state.isRuning) {
        //     setTimeout(() => {
        //         this.socket.emit("FruitNotMatch", (cb) => {
        //             console.log("cb nothing from server: " + cb)
        //         })

        //     }, 1000)
            
        // }
        if (this.state.cntr == 250 ) {

            let flag = true

            if (flag) {

                this.socket.emit("FruitNotMatch", (cb) => {
                    console.log("cb nothing from server: " + cb)
                })
                flag = false
            }

            this.setState({
                cntr: 0
            })
        }
        else {

            this.setState((prevState) => ({
                cntr: prevState.cntr + 1
            }))
        }
    }

    // handle the start button on the camera menu options so that the camera start recording
    handleStartBtn() {
        this.trackColors()
    }

    render() {

        return (
            <div >
                <Camera videoRef={el => this.cameraRefs = el} canvasRef={el => this.canvasRefs = el} vidSourceOnChange={null} />
                <CamCtrlMenu handleStartBtn={this.handleStartBtn} handleStopBtn={null} />
            </div>
        )
    }
}

const stateToProps = (state) => {
    return {
        section: state.section
    }
}

const dispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(stateToProps, dispatchToProps)(CameraField)