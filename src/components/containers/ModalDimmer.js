import React, { Component } from 'react'
import { connect } from 'react-redux'
import actions from '../../actions'
import { Popup, Button, Header, Modal, Divider } from 'semantic-ui-react'
import { ConfigSegment } from '../presentation'
import io from 'socket.io-client'

class ModalDimmer extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      stepperPins: [],
      arduinoPort: "",
      servoTubePins: [],
      servoGatePin: null
    }

    this.show = dimmer => () => this.setState({ dimmer, open: true })
    this.close = () => this.setState({ open: false })
    this.addStepperPins = this.addStepperPins.bind(this)
    this.addArduinoPort = this.addArduinoPort.bind(this)
    this.addServoGatePin = this.addServoGatePin.bind(this)
    this.addServoTubePins = this.addServoTubePins.bind(this)
    this.addConfDataToStore = this.addConfDataToStore.bind(this)

  }
  componentDidMount() {
    this.socket = io(`localhost:3000`, {
      transports: ['websocket'],
      upgrade: false,

    })
  }

  // add the stepper pins user inputed, to the backend on arduino board
  addStepperPins(e) {
    let stepperPins = Object.assign({}, this.state.stepperPins)
    stepperPins = e.target.value

    this.setState({
      stepperPins: stepperPins
    })
  }

  // add the servo pins on the tube construction that user inputed, to the backend on arduino board
  addServoTubePins(e) {
    let servoTubePins = Object.assign({}, this.state.servoTubePins)
    servoTubePins = e.target.value

    this.setState({
      servoTubePins: servoTubePins
    })
  }

  // add the servo pin on the bottom of the recognition box construction that user inputed, to the backend on arduino board
  addServoGatePin(e) {
    let servoGatePin = Object.assign({}, this.state.servoGatePin)
    servoGatePin = e.target.value

    this.setState({
      servoGatePin: servoGatePin
    })
  }

  // Add user's input on USB port to the arduino board
  addArduinoPort(e) {
    let arduinoPort = Object.assign({}, this.state.arduinoPort)
    arduinoPort = e.target.value

    this.setState({
      arduinoPort: arduinoPort
    })
  }

  // add all the user's config input to the store
  addConfDataToStore(e) {
    e.preventDefault()

    this.props.addArduinoPort(this.state.arduinoPort)
    this.props.addStepperPins(this.state.stepperPins)
    this.props.addServoTubePins(this.state.servoTubePins)
    this.props.addServoGatePin(this.state.servoGatePin)

    var { arduinoPort, servoGatePin, servoTubePins, stepperPins } = this.state
    servoTubePins = servoTubePins.split(',').map(servoTubePin => {
      return parseInt(servoTubePin, 10)
    })
    stepperPins = stepperPins.split(',').map(stepperPin => {
      return parseInt(stepperPin, 10)
    })
    servoGatePin = parseInt(servoGatePin, 10)
    this.socket.emit("arduinoConfig", arduinoPort, servoGatePin, servoTubePins, stepperPins, (cb) => {
      console.log("cb from server: " + cb)
    })
    this.close()
  }

  render() {
    const { open, dimmer, stepperPins, servoGatePin, servoTubePins, arduinoPort } = this.state

    return (
      <div  >
        <Popup trigger={<Button circular corner="true" size="tiny" onClick={this.show("blurring")} icon="configure" />}>
          <Popup.Header>Configure first!</Popup.Header>
          <Popup.Content>
            This is the configuration menu for setting up your arduino board plus your motors (servo,stepper).
            You have to configure your board and your motors before starting the application.
          </Popup.Content>
        </Popup>

        <Modal style={{ top: "1%", bottom: "1%" }} dimmer={dimmer} open={open} onClose={this.close}>
          <Modal.Header>Configurations</Modal.Header>

          <Modal.Content scrolling>
            <Modal.Description>
              <Header>Board - Servo - Stepper configuration</Header>
              <div>Please fill in all the input fields and apply your changes.</div>
              <div>By applying your changes, you setting up your arduino Board and your motors, letting the application to work properly</div>
              <Divider />
              <ConfigSegment addStepperPins={this.addStepperPins} addServoTubePins={this.addServoTubePins} addArduinoPort={this.addArduinoPort} addServoGatePin={this.addServoGatePin} />
            </Modal.Description>
          </Modal.Content>

          <Modal.Actions>
            <Button color='black' onClick={this.close}>
              Cancel
            </Button>
            
            <Button positive icon='checkmark' labelPosition='right' content="Apply" onClick={this.addConfDataToStore} />
          </Modal.Actions>
        </Modal>

      </div>
    )
  }
}

const stateToProps = (state) => {
  return {

  }
}

const dispatchToProps = (dispatch) => {
  return {
    addStepperPins: (stepperPins) => dispatch(actions.addStepperPins(stepperPins)),
    addServoTubePins: (servoTubePins) => dispatch(actions.addServoTubePins(servoTubePins)),
    addServoGatePin: (servoGatePin) => dispatch(actions.addServoGatePin(servoGatePin)),
    addArduinoPort: (arduinoPort) => dispatch(actions.addArduinoPort(arduinoPort))
  }
}

export default connect(stateToProps, dispatchToProps)(ModalDimmer)