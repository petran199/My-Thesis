import React, { Component } from 'react'
import { Input, Menu } from 'semantic-ui-react'
import { ModalDimmer } from './index'

export default class LoginMenu extends Component {
  constructor() {
    super()
    this.state = {
      activeItem: null
    }
    this.handleLogOut = (e, { name }) => this.setState({ activeItem: name })
  }

  render() {
    const { activeItem } = this.state

    return (
      <Menu secondary>
        <ModalDimmer />
        {/* <Menu.Menu position='right'>
          <Menu.Item>
            <Input icon='search' placeholder='Search...' />
          </Menu.Item>
          <Menu.Item name='logout' active={activeItem === 'logout'} onClick={this.handleLogOut} />
        </Menu.Menu> */}
      </Menu>
    )
  }
}
