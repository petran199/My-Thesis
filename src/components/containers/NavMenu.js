import React, { Component } from 'react'
import { connect } from 'react-redux'
import actions from '../../actions'
import { Nav } from '../presentation'


class NavMenu extends Component {

    constructor(props) {
        super(props)

        this.state = {
            activeMenuItem: window.currentPath

        }
        this.handleMenuItemClick = this.handleMenuItemClick.bind(this)
    }

    // handle user's click onto navigation menu
    handleMenuItemClick(e) {

        let activeMenuItem = Object.assign({}, this.state.activeMenuItem)
        activeMenuItem = e.target.id

        this.setState({
            activeMenuItem: activeMenuItem
        })
    }

    render() {
        let { activeMenuItem } = this.state
        let { handleMenuItemClick } = this
        let { items } = this.props.nav
        return (
            <div>
                <Nav items={items} handleMenuItemClick={handleMenuItemClick} activeMenuItem={activeMenuItem} />
            </div>
        )
    }

}

const stateToProps = (state) => {
    return {
        nav: state.nav
    }
}

const dispatchToProps = (dispatch) => {
    return {
        AddNavItems: (params) => dispatch(actions.AddNavItems(params))
    }
}

export default connect(stateToProps, dispatchToProps)(NavMenu)