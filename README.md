# Diploma Thesis Project

<p align="center">
  <img src="assets/images/teikav.png" width="100" height="100"/>
</p>

---

# Title:

## Autonomus production unit that sorts objects using computer vision with cretirion the color

### Run the project using Docker

###### You can run this project easily by using its docker image.  With this approach you avoid the installation of a huge list of dependencies required. The only requirement is to have the docker installed on your machine. 


1. [Download](https://docs.docker.com/install/#supported-platforms) and install Docker daemon to your machine.

2. Download My-thesis Docker image

    You can Download the project's Docker image by typing:

    `$ docker pull petran199/diploma-thesis`

3. Run project's Docker image on your machine

    You can run the project using its Docker image by opening a terminal and typing:

    `$ docker run -d -p 3000:3000 --rm --device=/dev/ttyUSB0 petran199/diploma-thesis`

     For the --device flag, you have to find out what's the USB port that your Arduino device uses.

4. Stop Docker image running on background

    When you have finished working on this project, it's good idea to stop docker image from running on background and release the specified port back. To do that, just type:

    `$ docker stop diploma-thesis`


### Instalation Guide for Linux - Windows Users

#### Linux Users

1. Download Arduino IDE [here](https://www.arduino.cc/en/Main/Software)

    We need that so we can find out the USB port that Arduino uses, and to upload the firmata code to initialise it and prepare it for use.

2. Clone or download [AdvancedFirmata](https://github.com/soundanalogous/AdvancedFirmata)

    If you download rather than clone this repository, rename the folder to "AdvancedFirmata" after you have unzipped it, then copy that folder into your Arduino projects directory.

    Copy the code into your Arduino projects/sketch directory (or anywhere on your hard drive). Open in the Arduino IDE, verify and upload AdvancedFirmata code to your board.

3. Download and install [Nodejs](https://nodejs.org/en/download/)


4. Add your user to dialout group if it's not already there.

    $ sudo usermod -a -G dialout _username_

    e.g. 

    `$ sudo usermod -a -G dialout $USER`

    Reboot after that so changes can take effect.

5. Allow terminal having access on serial.

    First make sure you have connected the Arduino board with your PC.

    $ sudo chmod a+rw /dev/ttyUSB*

    where * is the number of the port where Arduino board has connected

    e.g.

    `$ sudo chmod a+rw /dev/ttyUSB0`

6. Make sure you have installed [Nodejs](https://nodejs.org/en/download/)

7. Clone or Download [My-Thesis project](https://gitlab.com/petran199/My-Thesis)

8. Install required dependencies

    Open a terminal, navigate to the project you cloned or downloaded and just install all the dependencies by typing:

    `$ sudo npm i -g webpack@3.10.0`

    `$ sudo npm i -g gulp`

    `$ npm i`

9. Run the project on your machine

    Open a terminal and navigate to the project, then just type npm start to let the server run. After that open a web browser of your preference and go to [http://localhost:3000/home/dashboard](http://localhost:3000/home/dashboard)


#### Windows Users


1. Download and install Arduino IDE [here](https://www.arduino.cc/en/Main/Software)

    We need that so we can find out the USB port that Arduino uses, and to upload the firmata code to initialise it and prepare it for use.

2. Clone or download [AdvancedFirmata](https://github.com/soundanalogous/AdvancedFirmata)

    If you download rather than clone this repository, rename the folder to "AdvancedFirmata" after you have unzipped it, then copy that folder into your Arduino projects directory.

    Copy the code into your Arduino projects/sketch directory (or anywhere on your hard drive). Open in the Arduino IDE, verify and upload AdvancedFirmata code to your board.

3. Download and install [Nodejs](https://nodejs.org/en/download/)

4. Clone or Download [My-Thesis project](https://gitlab.com/petran199/My-Thesis)

5. Type some commands that are required to build all the dependencies

    Open powershell with administrator rights, navigate to the project you have cloned or downloaded and type:

    `$ npm install --global --production windows-build-tools`

    `$ npm i -g webpack@3.10.0`

    `$ npm i -g gulp`

    Open powershell without administrator rights, navigate to the project you have cloned or downloaded and type:

    `$ npm i firmata`

    `$ npm i`

    `$ npm rebuild serialport --build-from-source=serialport`

    `$ webpack`

    `$ gulp prod`

6. Run the project on your machine

    Open a terminal and navigate to the project, then just type npm start to let the server run. After that open a web browser of your preference and go to [http://localhost:3000/home/dashboard](http://localhost:3000/home/dashboard)

---
